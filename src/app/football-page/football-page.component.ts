import { Component, OnInit } from '@angular/core';

import { FootDbApiService } from '../foot-db-api.service';
import { NotificationService } from '../notification.service';
import { LiveService } from '../live.service';

@Component({
  selector: 'app-football-page',
  templateUrl: './football-page.component.html',
  styleUrls: ['./football-page.component.css']
})
export class FootballPageComponent implements OnInit {
  data: string[];
  wcup: string[];
  loaded = false;
  loadedWc = false;
  constructor(private footApi: FootDbApiService, private notifSrv: NotificationService, private liveSrv: LiveService) {
  }

  onDisplay() {
    console.log(this.data);
  }

  ngOnInit() {
    // current season
    this.footApi.getObsRequest('competitions/').subscribe(data => {
      this.data = data;
      this.notifSrv.notify('Les données de l\'API FootballData.org ont été chargées avec succès', 'SUCCESS', 3000);
      this.loaded = true;
    });
    this.footApi.getObsRequest('competitions/467').subscribe(data => {
      this.wcup = data;
      console.log('World Cup data', this.wcup);
      this.notifSrv.notify('Les données world Cup de l\'API FootballData.org ont été chargées avec succès', 'SUCCESS', 3000);
      this.loadedWc = true;
    });
  }

  showLive() {
    this.liveSrv.show = true;
  }

}
