# MySite

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.1.
This site give you an access and user interface to different api, now only the football api database is developped
this is the link to this api please visit the following link to get more informations
This site is also plugged to a backend node js server that i've also developped.
This is project contains only the front end part of my project.

[api football data](http://api.football-data.org)

[Visit the production Site](http://vps336197.ovh.net)
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Deployment

Test server Deployment:
[Visit the Beta Site](http://football-api-site-stage.surge.sh)

Production Site:
[Visit the production Site](http://vps336197.ovh.net)

Last deployment status:
[![pipeline status](https://gitlab.com/Guill76/football-api-site/badges/master/pipeline.svg)](https://gitlab.com/Guill76/football-api-site/commits/master)


## Running unit tests

Most of the tests are in development, some components are full covered, but some ot them are not
covered by tests.

Last code coverage status:
[![coverage report](https://gitlab.com/Guill76/football-api-site/badges/master/coverage.svg)](https://gitlab.com/Guill76/football-api-site/commits/master)

[coverage](http://football-api-site-coverage.surge.sh)

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
