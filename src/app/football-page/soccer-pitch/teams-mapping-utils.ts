export class TeamsPositionMapping {
    mapping: any [];
    constructor (private widthIRL, private heightIRL, private width, private top) {
        const scale = this.widthIRL / this.width;
        this.mapping = [
            {
                pos: 'Keeper',
                coordsUp: [{
                    meterX: this.widthIRL / 2,
                    meterY: this.heightIRL,
                    x: 0,
                    y: 0
                }],
                coordsDown: [{
                    meterX: this.widthIRL / 2,
                    meterY: 0,
                    x: 0,
                    y: 0
                }],
                readyFlag: false
            },
            {
                pos: 'Right-Back',
                coordsUp: [{
                    meterX: 5 * this.widthIRL / 6,
                    meterY: 8 * this.heightIRL / 9,
                    x: 0,
                    y: 0
                }],
                coordsDown: [{
                    meterX: this.widthIRL / 6,
                    meterY: this.heightIRL / 9,
                    x: 0,
                    y: 0

                }],
                readyFlag: false
            },
            {
                pos: 'Left-Back',
                coordsUp: [{
                    meterX:  this.widthIRL / 6,
                    meterY: 8 * this.heightIRL / 9,
                    x: 0,
                    y: 0
                }],
                coordsDown: [{
                    meterX: 5 * this.widthIRL / 6,
                    meterY: this.heightIRL / 9,
                    x: 0,
                    y: 0
                }],
                readyFlag: false
            },
            {
                pos: 'Centre-Back',
                coordsUp: [
                    {
                        meterX: 3 * this.widthIRL / 8,
                        meterY: 8 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: 5 * this.widthIRL / 8,
                        meterY: 8 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: this.widthIRL / 2,
                        meterY: 8 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    }
                ],
                coordsDown: [
                    {
                        meterX: 5 * this.widthIRL / 8,
                        meterY: this.heightIRL / 9,
                        x: 0,
                        y: 0

                    },
                    {
                        meterX: 3 * this.widthIRL / 8,
                        meterY: this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: this.widthIRL / 2,
                        meterY: this.heightIRL / 9,
                        x: 0,
                        y: 0
                    }
                ],
                readyFlag: false
            },
            // Defensive Midfield
            {
                pos: 'Defensive Midfield',
                coordsUp: [
                    {
                        meterX: 4 * this.widthIRL / 6,
                        meterY: 7 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: 2 * this.widthIRL / 6,
                        meterY: 7 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: this.widthIRL / 2,
                        meterY: 7 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    }
                ],
                coordsDown: [
                    {
                        meterX: 4 * this.widthIRL / 6,
                        meterY: 2 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: 2 * this.widthIRL / 6,
                        meterY: 2 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: this.widthIRL / 2,
                        meterY: 2 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    }
                ],
                readyFlag: false
            },
            // Central Midfield
            {
                pos: 'Central Midfield',
                coordsUp: [
                    {
                        meterX: 4 * this.widthIRL / 6,
                        meterY: 6 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: 2 * this.widthIRL / 6,
                        meterY: 6 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: this.widthIRL / 2,
                        meterY: 6 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    }
                ],
                coordsDown: [
                    {
                        meterX: 4 * this.widthIRL / 6,
                        meterY: 3 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: 2 * this.widthIRL / 6,
                        meterY: 3 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: this.widthIRL / 2,
                        meterY: 3 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                ],
                readyFlag: false
            },
             // Attacking Midfield
            {
            pos: 'Attacking Midfield',
            coordsUp: [
                {
                    meterX: 1 * this.widthIRL / 6,
                    meterY: 5 * this.heightIRL / 9,
                    x: 0,
                    y: 0
                },
                {
                    meterX: 5 * this.widthIRL / 6,
                    meterY: 5 * this.heightIRL / 9,
                    x: 0,
                    y: 0
                },
                {
                    meterX: this.widthIRL / 2,
                    meterY: 5 * this.heightIRL / 9,
                    x: 0,
                    y: 0
                }
            ],
            coordsDown: [
                {
                    meterX: 1 * this.widthIRL / 6,
                    meterY: 4 * this.heightIRL / 9,
                    x: 0,
                    y: 0
                },
                {
                    meterX: 5 * this.widthIRL / 6,
                    meterY: 4 * this.heightIRL / 9,
                    x: 0,
                    y: 0
                },
                {
                    meterX: this.widthIRL / 2,
                    meterY: 4 * this.heightIRL / 9,
                    x: 0,
                    y: 0
                }
            ],
            readyFlag: false
            },
            // Right-Wing
            {
                pos: 'Right Wing',
                coordsUp: [
                    {
                        meterX: 5 * this.widthIRL / 6,
                        meterY: 5 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    }
                ],
                coordsDown: [
                    {
                        meterX: this.widthIRL / 6,
                        meterY: 4 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    }
                ],
                readyFlag: false
            },
            // Left-Wing
            {
                pos: 'Left Wing',
                coordsUp: [
                    {
                        meterX: this.widthIRL / 6,
                        meterY: 5 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    }
                ],
                coordsDown: [
                    {
                        meterX: 5 * this.widthIRL / 6,
                        meterY: 4 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    }
                ],
                readyFlag: false
            },
            // Centre-Forward
            {
                pos: 'Centre-Forward',
                coordsUp: [
                    {
                        meterX: 2 * this.widthIRL / 6,
                        meterY: 5 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: 4 * this.widthIRL / 6,
                        meterY: 5 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: this.widthIRL / 2,
                        meterY: 5 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    }
                ],
                coordsDown: [
                    {
                        meterX: 2 * this.widthIRL / 6,
                        meterY: 4 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: 4 * this.widthIRL / 6,
                        meterY: 4 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    },
                    {
                        meterX: this.widthIRL / 2,
                        meterY: 4 * this.heightIRL / 9,
                        x: 0,
                        y: 0
                    }
                ],
                readyFlag: false
            }
        ];

        this.mapping.forEach( (it) => {
            for (let i = 0; i < it.coordsDown.length; i++) {
                it.coordsDown[i].x = (it.coordsDown[i].meterX / scale) + top.x;
                it.coordsDown[i].y = (it.coordsDown[i].meterY / scale) + top.y;
                it.coordsUp[i].x = (it.coordsUp[i].meterX / scale) + top.x;
                it.coordsUp[i].y = (it.coordsUp[i].meterY / scale) + top.y;
            }
            // console.log('check coord xy', this.mapping);
        });
    }
}
