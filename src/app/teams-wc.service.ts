import { Injectable } from '@angular/core';
import { Observable, Subscription, pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfigService} from './config.service';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class TeamsWCService {
  _resultData: any;
  constructor(private http: HttpClient, private config: ConfigService) {

  }

  getTeams(url: string): Observable<any> {
    return this.http.get(this.config.baseUrlApi + url, {responseType: 'json'});
  }

  loadOnce(url) {
    this.getTeams(url).pipe(map(res => res.results)).subscribe((res) => {
      this._resultData = res;
    },
    (err) => {
      console.error(err);
    }
  );
  }
  get data () {
    return this._resultData;
  }
}
