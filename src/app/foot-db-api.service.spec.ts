import { TestBed, inject } from '@angular/core/testing';

import { FootDbApiService } from './foot-db-api.service';
import { NotificationService } from './notification.service';
import {
  HttpClientModule
} from '@angular/common/http';


describe('FootDbApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [FootDbApiService, NotificationService]
    });
  });

  it('should be created', inject([FootDbApiService], (service: FootDbApiService) => {
    expect(service).toBeTruthy();
  }));
});
