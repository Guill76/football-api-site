import { Observable ,  Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders} from '@angular/common/http';
import { ConfigService} from './config.service';
import { NotificationService } from './notification.service';
import { WebsocketService } from './websocket.service';

@Injectable()
export class UsersService {
  users;
  datas;
  wsSub$: Subject<any>; // websocketSubject
  errorMsg;
  connectionMessage;
  connected: string[] = [];
  constructor(private http: HttpClient, public config: ConfigService, public ws: WebsocketService, public notifSrv: NotificationService) {
  }

  authenticate(user): Observable<any> {
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(this.config.baseUrlApi + '/api/auth', user);
  }
  addUser(user): Observable<any> {
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(this.config.baseUrlApi + '/api/user', user);
  }
  updateUser(user): Observable<any> {
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.put(this.config.baseUrlApi + '/api/user', user);
  }
  deleteUser(user): Observable<any> {
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.delete(this.config.baseUrlApi + '/api/users/' + user._id);
  }

  getUser(name: string): Observable<any> {
    return this.http.get(this.config.baseUrlApi + '/api/users/' + name);
  }

  getAllUsers() {
     return this.http.get(this.config.baseUrlApi + '/api/allusers/');
  }

  getRole() {
    // console.log(this.users.role);
    if (this.users) {
      return this.users.role;
    } else {
      return null;
    }
  }

  setConnected() {
    if (this.users) {
      this.users.isConnected = true;
      this.users.password = '';
      localStorage.setItem('key-meanapp', JSON.stringify(this.users));
      this.wsSub$ = this.ws.connect(`${this.config.wsUrl}`);
      setTimeout( () => {
        this.connectionMessage = {
          type: 'login',
          value: this.getConnectedUser()
        };
        this.wsSub$.next(this.connectionMessage);
      }, 100);
      this.wsSub$.subscribe((resp) => {
        console.log('websocket', resp);
        if (resp.type === 'sINFO') {
          this.notifSrv.notify(resp.value, 'INFO', 3000);
        } else if ( resp.type === 'aUSERS') {
          if (resp.value.length > 0) {
            console.log('vidage du tableau', this.connected);
            this.connected.splice(0, this.connected.length);
            console.log('Tableau vidé', this.connected);

            resp.value.forEach(user => {
              this.connected.push(user);
              console.log(this.connected);
            });
          }
        }
      },
      err => console.log('WS errors', err));
    }
  }

  loadFromCache() {
    this.users = JSON.parse(localStorage.getItem('key-meanapp'));
    setTimeout( () => this.setConnected(), 0 );
  }

  getConnectedUser() {
    return (this.users ? this.users.username : null);
  }

  deconnect ( ) {
    // on repasse tout à false car ce service n'est propre qu'au gars connecté
    this.users.isConnected = false;
    localStorage.removeItem('key-meanapp');
  }

  isConnected ( ): boolean {
     return (this.users && this.users.isConnected);
  }
}
