import { Injectable } from '@angular/core';
import { HttpClient, HttpParams} from '@angular/common/http';

import { ConfigService } from './config.service';

@Injectable()
export class SoccerPitchService {

  constructor(private http: HttpClient, public config: ConfigService) {
  }
  private extractData(res: Response) {
    const body = res.json();
    return body || { };
  }

  postProperties(props) {
    return this.http.post(this.config.baseUrlApi + '/api/soccerPitch', props);
  }
  postTeamConfigProperties(props) {
    return this.http.post(this.config.baseUrlApi + '/api/teamsProperties', props);
  }
  getProperties() {
    return this.http.get(this.config.baseUrlApi + '/api/soccerPitch');
  }
  getTeamConfigProperties(id) {
    return this.http.get(this.config.baseUrlApi + '/api/teamsProperties/' + id);
  }
  updateTeamConfigProperties(teamPropertyObject) {
    return this.http.put(this.config.baseUrlApi + '/api/teamsProperties', teamPropertyObject);
  }
  deleteProperties(id) {

    return this.http.delete(this.config.baseUrlApi + '/api/soccerPitch/' + id);
  }
  updateProperties(propertyObject) {
    return this.http.put(this.config.baseUrlApi + '/api/soccerPitch', propertyObject);
  }


}
