import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NotificationService } from './notification.service';

@Injectable()
export class FootDbApiService {
  readonly apiUrl: string = 'http://api.football-data.org/v1/' ;
  // readonly apiToken : string ='3d2ff424c0454c4cbb1b6d182bd16f8e';
  readonly apiToken: string = '3d2ff424c0454c4cbb1b6d182bd16f8e';
  loaded: Boolean = false;
  resObs: Observable<any>;
  results: string[];
  constructor(private http: HttpClient, private notifServ: NotificationService) { }

  getObsRequest(name: string, paramXRespControl:  string = 'minified') {
    const options = new HttpHeaders()
      .set('X-Auth-Token', this.apiToken)
      .set('X-Response-Control', paramXRespControl);
    this.resObs = this.http.get(this.apiUrl + name,
      {
        headers: options,
        responseType: 'json'
      });
    // console.log ('Real foot-db');
    return this.resObs;
  }
  loadData(name: string) {
    this.loaded = false;
    this.results = null;
    this.getObsRequest(name);
    try {
    this.resObs.subscribe(data => {
      this.results = data;
      this.notifServ.notify('Les données de l\'API Foot Data ont été chargées avec succès', 'SUCCESS', 3000);
      this.loaded = true;
    });
    } catch (err) {
      this.notifServ.notify(err.message, 'SUCCESS', 3000);
    }
  }
}
